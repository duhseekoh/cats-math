import Expo from 'expo';
import React from 'react';
import RootNavigator from './screens/RootNavigator';

class App extends React.Component {
  render() {
    return <RootNavigator />;
  }
}

Expo.registerRootComponent(App);
