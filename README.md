# Cats Math

An app that calculates the number of cats you should have!

## Tech

- expo.io

## Install

1. go through setup steps at expo.io
1. clone this repo
1. open the project in the expo xde
1. choose to launch on the ios simulator from expo xde
1. open the project in your favorite editor
1. explore and make changes
