const colors = {
  white: '#FFFFFF',
  // reds
  pink: '#E4746E',
  red: '#BD3F38',
  darkRed: '#680500',
  // blues
  lightBlue: '#8FA5BE',
  blue: '#2B517B',
  darkBlue: '#042244',
  // greens
  paleGreen: '#EEF9B6',
  lightGreen: '#CBDD6A',
  green: '#A2B736',
  darkGreen: '#546400'
};

const padding = {
  edgeOfScreen: 20,
};

export {
  colors,
  padding,
}
