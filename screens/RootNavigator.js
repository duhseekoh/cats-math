import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import WelcomeScreen from './WelcomeScreen';
import CalculatorScreen from './CalculatorScreen';

const MainScreenStack = StackNavigator({
  Welcome: {
    screen: WelcomeScreen,
    navigationOptions: () => ({
      header: null,
    }),
  },
  Calculator: {
    screen: CalculatorScreen,
    navigationOptions: () => ({
      title: 'Catulator',
    }),
  },
}, {
  headerMode: 'screen'
});

const RootTabNavigator = TabNavigator({
  MainTab: {
    screen: MainScreenStack,
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarIcon: ({ tintColor }) => <MaterialCommunityIcons name='calculator' size={24} color={tintColor} />
    },
  },
  // can add another Tab here
});

export default RootTabNavigator;
