import React from 'react';
import { Animated, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as styleConsts from '../styles/constants';

const AnimatedMaterialCommunityIcons = Animated.createAnimatedComponent(MaterialCommunityIcons);

class CalculatorScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfPeopleInput: '0', // form value
      numberOfRoomsInput: '0',  // form value
      isSubmitted: false,
      numberOfCatsOutput: 0,
      catCountIconBottom: new Animated.Value(-300),
    };

    this._onPressCalculate = this._onPressCalculate.bind(this);
    this._animateCatPrize = this._animateCatPrize.bind(this);
  }

  componentDidMount() {
  }

  _onPressCalculate() {
    const { numberOfPeopleInput: ceiling, numberOfRoomsInput } = this.state;

    const rawNumberOfCats = Math.floor(numberOfRoomsInput / 2);
    const notTooBig = rawNumberOfCats >= ceiling ? ceiling : rawNumberOfCats;
    const notTooSmall = notTooBig || 1;

    this.setState({
      numberOfCatsOutput: notTooSmall,
      isSubmitted: true,
    });

    this._animateCatPrize(notTooSmall);
  }

  _animateCatPrize(numberOfCats) {
    Animated.loop(Animated.sequence([
      Animated.timing(
        this.state.catCountIconBottom, { toValue: -50 },
      ),
    ]), { iterations: numberOfCats }).start();
  }

  render() {
    const { numberOfPeopleInput,
      numberOfRoomsInput,
      numberOfCatsOutput,
      isSubmitted,
    } = this.state;

    return (
      <ScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <View style={styles.form}>
            <View>
              <Text style={styles.questionText}>How many bedrooms in your home?</Text>
              <View>
                <TextInput style={styles.answerInput}
                  autoFocus={true}
                  selectTextOnFocus={true}
                  keyboardType='numeric'
                  maxLength={2}
                  value={numberOfRoomsInput}
                  onChangeText={(text) => this.setState({
                    numberOfRoomsInput: text,
                    isSubmitted: false,
                  })} />
              </View>
            </View>
            <View>
              <Text style={styles.questionText}>Number of people that live there?</Text>
              <View>
              <TextInput style={styles.answerInput}
                keyboardType='numeric'
                selectTextOnFocus={true}
                maxLength={2}
                value={numberOfPeopleInput}
                onChangeText={(text) => this.setState({
                  numberOfPeopleInput: text,
                  isSubmitted: false,
                })} />
              </View>
            </View>
            <TouchableOpacity style={styles.calculateButton} onPress={this._onPressCalculate}>
              <Text style={styles.calculateButtonText}>CATULATE IT</Text>
            </TouchableOpacity>
          </View>
          { isSubmitted &&
          <View style={styles.output}>
            <Text style={styles.outputText}>
              <Text style={styles.numbersOfCatsLabel}>You get </Text>
              <Text style={styles.numberOfCats}>{numberOfCatsOutput}</Text>
              <Text style={styles.numbersOfCatsLabel}> cats!</Text>
            </Text>
            <AnimatedMaterialCommunityIcons
              style={[styles.catCountIcon]}
              name='cat'
              color={styleConsts.colors.darkGreen} />
            <AnimatedMaterialCommunityIcons
              style={[styles.catCountIcon, { bottom: this.state.catCountIconBottom }]}
              name='cat'
              color={styleConsts.colors.darkGreen} />
          </View>
          }
        </View>
      </ScrollView>
    );
    //https://twitter.com/duhseekoh/status/760208647079202816
  }
}

export default CalculatorScreen;

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: styleConsts.colors.white,
  },
  container: {
    flex: 1,
    paddingTop: 20,
    alignItems: 'center',
  },
  questionText: {
    fontWeight: 'bold',
    color: styleConsts.colors.pink,
  },
  answerInput: {
    height: 70,
    width: 125,
    fontSize: 60,
    marginTop: 10,
    marginBottom: 20,
    borderWidth: 4,
    borderColor: styleConsts.colors.blue,
    borderRadius: 5,
    backgroundColor: styleConsts.colors.blue,
    color: styleConsts.colors.white,
    paddingHorizontal: 20,
    paddingVertical: 8,
    textAlign: 'center',
    alignSelf: 'center',
  },
  calculateButton: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: styleConsts.colors.white,
    borderWidth: 5,
    borderColor: styleConsts.colors.green,
    shadowColor: '#000000',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: .2,
  },
  calculateButtonText: {
    fontSize: 30,
    color: styleConsts.colors.darkGreen,
  },
  output: {
    marginTop: 20,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  outputText: {
    color: styleConsts.colors.darkBlue,
  },
  numbersOfCatsLabel: {
    fontSize: 24,
  },
  numberOfCats: {
    fontSize: 50,
  },
  catCountIcon: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: -50,
    fontSize: 50,
  },
});
