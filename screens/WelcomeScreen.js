import React, { PropTypes } from 'react';
import { Animated, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as styleConsts from '../styles/constants';

const AnimatedMaterialCommunityIcons = Animated.createAnimatedComponent(MaterialCommunityIcons);

class WelcomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonCatSize: new Animated.Value(30),
      catRowOpacity: new Animated.Value(0),
    };

    this._onPressGetStarted = this._onPressGetStarted.bind(this);
  }

  componentDidMount() {
    this._animateCatRowOpacity();
    this._animateButtonCat();
  }

  _animateCatRowOpacity() {
    Animated.loop(Animated.sequence([
      Animated.spring(
        this.state.catRowOpacity, { toValue: 1 },
      ),
      Animated.spring(
        this.state.catRowOpacity, { toValue: .3 },
      )
    ])).start();
  }

  _animateButtonCat() {
    Animated.loop(Animated.sequence([
      Animated.timing(
        this.state.buttonCatSize, { toValue: 100 },
      ),
      Animated.timing(
        this.state.buttonCatSize, { toValue: 30 },
      )
    ])).start();
  }

  _onPressGetStarted() {
    this.props.navigation.navigate('Calculator');
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.welcome}>
          <Text style={styles.welcomeMessage}>Cats Math</Text>
          <Animated.View style={[styles.welcomeCats, {opacity: this.state.catRowOpacity}]}>
            <MaterialCommunityIcons name='cat' size={35} color={styleConsts.colors.white} />
            <MaterialCommunityIcons name='calculator' size={35} color={styleConsts.colors.green} />
            <MaterialCommunityIcons name='cat' size={35} color={styleConsts.colors.pink} />
            <MaterialCommunityIcons name='calculator' size={35} color={styleConsts.colors.red} />
            <MaterialCommunityIcons name='cat' size={35} color={styleConsts.colors.blue} />
            <MaterialCommunityIcons name='calculator' size={35} color={styleConsts.colors.darkBlue} />
            <MaterialCommunityIcons name='cat' size={35} color={styleConsts.colors.white} />
            <MaterialCommunityIcons name='calculator' size={35} color={styleConsts.colors.darkGreen} />
            <MaterialCommunityIcons name='cat' size={35} color={styleConsts.colors.darkRed} />
            <MaterialCommunityIcons name='calculator' size={35} color={styleConsts.colors.blue} />
            <MaterialCommunityIcons name='cat' size={35} color={styleConsts.colors.white} />
            <MaterialCommunityIcons name='calculator' size={35} color={styleConsts.colors.darkBlue} />
          </Animated.View>
        </View>
        <Text style={styles.description}>
          <Text style={styles.descriptionLead}>Ever had trouble</Text> deciding how many cats
          you can have living in your home before it becomes socially unacceptable?
          <Text style={styles.descriptionTail}> Let us run the numbers.</Text>
        </Text>
        <TouchableOpacity
          onPress={this._onPressGetStarted}>
          <View style={styles.getStartedButton}>
            <Text style={styles.getStartedText}>Tap here to boot up the Cats Math Calculator!</Text>
            <AnimatedMaterialCommunityIcons style={{fontSize: this.state.buttonCatSize}} name='cat' color={styleConsts.colors.darkGreen} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

WelcomeScreen.propTypes = {
  navigation: PropTypes.object,
};

export default WelcomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: styleConsts.colors.white,
  },
  welcome: {
    alignItems: 'center',
    height: 120,
    maxWidth: '100%',
    paddingTop: 30,
    backgroundColor: styleConsts.colors.lightBlue,
  },
  welcomeMessage: {
    fontSize: 30,
    color: styleConsts.colors.white,
    borderBottomWidth: 5,
    borderColor: styleConsts.colors.red,
  },
  welcomeCats: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
  },
  description: {
    color: styleConsts.colors.darkBlue,
    padding: 40,
    fontSize: 20,
  },
  descriptionLead: {
    fontSize: 26,
    color: styleConsts.colors.darkRed,
  },
  descriptionTail: {
    fontSize: 23,
    color: styleConsts.colors.red,
  },
  getStartedButton: {
    width: 200,
    padding: 30,
    borderRadius: 10,
    backgroundColor: styleConsts.colors.white,
    borderWidth: 5,
    borderColor: styleConsts.colors.green,
    shadowColor: '#000000',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: .2,
    shadowRadius: 10,
    alignItems: 'center',
  },
  getStartedText: {
    color: styleConsts.colors.darkGreen,
    textAlign: 'center',
  }
});
